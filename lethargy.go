package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"syscall"
	"time"
	"unicode"
)

// [U]00[,00[]]h00[,00[]]m
// [U]00m
// [U]/00h/00m
// +231m => ogni 231 minuti dalla fine del precedente
// func to_number(str string) int {

// }

func check_digits(str string) bool {
	for _, c := range str {
		if !unicode.IsDigit(c) {
			return false
		}
	}
	return true
}

func get_next_time(input string, currentTime time.Time) time.Time {
	// Note: rune, byte array... may be here I have some bugs

	// if len(input)==0 {
	// 	impossible, bash does not allow this
	// }

	var location *time.Location

	if input[0] == '+' {
		minuti_str := input[1:]

		duration, err := time.ParseDuration(minuti_str)
		if err != nil {
			log.Fatal(err)
		}
		return currentTime.Add(duration)
	} else {
		if input[0] == 'U' {
			input = input[1:]
			location = time.UTC
		} else {
			location = currentTime.Location()
		}
	}

	h_idx := strings.Index(input, "h")
	if h_idx < 0 {
		log.Fatal("Hours symbol 'm' not found")
	}
	h_str, m_str := input[:h_idx], input[h_idx+1:]
	if len(m_str) == 0 {
		m_str = "00"
	} else {
		m_idx := strings.Index(m_str, "m")
		if m_idx < 0 {
			log.Fatal("Hours symbol 'h' not found")
		}
		if m_idx+1 != len(m_str) {
			log.Fatal("Time not in the form XXhYYm")
		}
		m_str = m_str[:m_idx]
	}
	h_int, err1 := strconv.Atoi(h_str)
	m_int, err2 := strconv.Atoi(m_str)
	if err1 != nil || err2 != nil || !check_digits(h_str) || !check_digits(m_str) || h_int < 0 || h_int > 23 || m_int < 0 || m_int > 59 {
		log.Fatal("Time not in the form DDhDDm (D=Digit)")
	}

	newTime := time.Date(currentTime.Year(), currentTime.Month(), currentTime.Day(),
		h_int, m_int, currentTime.Second(), 0, location)

	if newTime.Before(currentTime) {
		newTime = newTime.AddDate(0, 0, 1)

	}

	return newTime
}

func time_to_next_time(next_time_str string) time.Duration {
	current_time := time.Now()
	next_time := get_next_time(next_time_str, current_time)
	return next_time.Sub(current_time)
}

// FIX errore il currenttime deve essere los tesso del main

func main() {
	args := os.Args[1:]
	if len(args) < 1 {
		fmt.Println("Error: I need at least one argument")
		os.Exit(1)
	}

	// flag module is overkilled
	next_time_str := args[0]
	command_to_execute := args[1:]

	time2next_time := time_to_next_time(next_time_str)

	fmt.Println(time2next_time)
	fmt.Println(command_to_execute)
	fmt.Println(os.Environ())
	// There  can  be  no return from a successful execve because the calling core image is lost
	err := syscall.Exec(command_to_execute[0], command_to_execute, os.Environ())
	if err != nil {
		panic(err)
	}
	//time.Sleep(time2next_time)
	// fmt.Println(time.Date(2020, 1, 2, 3, 60, 5, 0, time.UTC))

}
